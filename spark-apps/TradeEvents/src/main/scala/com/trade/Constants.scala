package com.trade
import org.apache.spark.sql._
object Constants {
  val HDFS_INPUT_DIR = "/userapps/analytics/"
  val HIVE_SCHEMA_NAME = "analytics"
  val HIVE_TABLE_NAME = "trade_events_fct"
  val HIVE_TABLE_SAVE_MODE = SaveMode.Overwrite
  val HIVE_TABLE_FORMAT = "orc"
}
