package com.trade

import java.io.File

import org.apache.spark.rdd.RDD
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.slf4j.{Logger, LoggerFactory}
import java.sql.Timestamp
import org.joda.time.DateTime
import org.apache.spark.sql.functions._
import com.trade.model._

/**
  * Main class of Application - Trade Event Logger
  *
  */

object EventLoggerMain {

  val LOGGER: Logger = LoggerFactory.getLogger(EventLoggerMain.getClass)

  def main(args: Array[String]): Unit = {

    // Stage - 1 Initialization Starts
    // Finding warehouse location
    val warehouseLocation = new File("spark-warehouse").getAbsolutePath

    LOGGER.info("Initialize Spark Session")
    val spark: SparkSession = SparkSession
      .builder()
      .appName("Trade Event Logger")
      .config("spark.sql.warehouse.dir", warehouseLocation)
      .enableHiveSupport()
      .getOrCreate()

    LOGGER.info("Initialize SQL Context")
    val sqlContext = spark.sqlContext
    sqlContext.setConf("hive.exec.dynamic.partition", "true")
    sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")

    LOGGER.info("Initialize Spark Context")
//    spark.sparkContext.setLogLevel("WARN")
    val sc  = spark.sparkContext

    LOGGER.info("Reading files from HDFS location and creating RDD of Row")
    val rdd_input_files: RDD[Row] =sc.wholeTextFiles(Constants.HDFS_INPUT_DIR).map({case (name, contents) => Row(name.split("/").last, contents)})

    LOGGER.info("Creating Dataframe from RDD of Row")
    val df_input_files = spark.createDataFrame(rdd_input_files, StructType(List(StructField("file_name",StringType,true),StructField("content",StringType,true))))

    LOGGER.info("Exploding by \\n - trade delimiter ")
    val df_input_files_lines = df_input_files.withColumn("content", explode(split(df_input_files("content"), "[\n]")))

    LOGGER.info("Filtering out records with empty contents")
    val df_valid_trades = df_input_files_lines.filter("content != ''")

    LOGGER.info("Assigning unique seq number for each trade line")
    val df_trades_with_id = df_valid_trades.withColumn("rowId", monotonically_increasing_id())

    LOGGER.info("Creating a unique trade_id with file_name and auto generated seq number (file_name+'_'+index)")
    val df_trades_with_tid = df_trades_with_id.withColumn("trade_id",concat(df_trades_with_id("file_name"), lit("_"), df_trades_with_id("rowId")))

    LOGGER.info("Exploding by | - events delimiter")
    val df_trades_events = df_trades_with_tid.withColumn("content", explode(split(df_trades_with_tid("content"), "[|]")))

    LOGGER.info("Exploding by , - column delimiter")
    val df_trades_events_all_cols = df_trades_events
                                        .withColumn("product", trim(split(df_trades_events("content"), "\\,").getItem(0)))
                                        .withColumn("time_stamp", trim(split(df_trades_events("content"), "\\,").getItem(1)))
                                        .withColumn("amount", trim(split(df_trades_events("content"), "\\,").getItem(2)))
                                        .withColumn("record_insert_dt", lit(new Timestamp(new DateTime().getMillis)))

    LOGGER.info("Dropping columns not required in final DF")
    val df_trades_events_fct_cols = df_trades_events_all_cols.drop("rowId","content")

    LOGGER.info("Preparing final dataframe for writing hive table")
    val df_trades_events_fct = df_trades_events_fct_cols.select(
      df_trades_events_fct_cols("trade_id").as(trade_events_fct.trade_id),
      df_trades_events_fct_cols("product").as(trade_events_fct.product),
      df_trades_events_fct_cols("time_stamp").as(trade_events_fct.time_stamp),
      df_trades_events_fct_cols("amount").as(trade_events_fct.amount),
      df_trades_events_fct_cols("record_insert_dt").as(trade_events_fct.record_insert_dt),
      df_trades_events_fct_cols("file_name").as(trade_events_fct.file_name)
    )

//    LOGGER.info(trades_events_fct.show().toString)

    LOGGER.info("Writing data to hive table - trade_events_fct")
    df_trades_events_fct
      .write
      .mode(Constants.HIVE_TABLE_SAVE_MODE)
      .format(Constants.HIVE_TABLE_FORMAT)
      .insertInto(Constants.HIVE_SCHEMA_NAME+"."+Constants.HIVE_TABLE_NAME)

    System.exit(0)

  }
}