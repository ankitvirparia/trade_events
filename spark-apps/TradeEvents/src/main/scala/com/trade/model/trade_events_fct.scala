package com.trade.model

case object trade_events_fct{
  val product:String = "product"
  val time_stamp:String = "time_stamp"
  val amount:String = "amount"
  val record_insert_dt:String = "record_insert_dt"
  val file_name:String = "file_name"
  val trade_id:String = "trade_id"
}