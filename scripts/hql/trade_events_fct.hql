SET hive.exec.dynamic.partition = true;
SET hive.exec.dynamic.partition.mode = nonstrict;

USE ANALYTICS;

DROP TABLE TRADE_EVENTS_FCT;

CREATE TABLE TRADE_EVENTS_FCT (
        trade_id string,
	product string,
        time_stamp string,
        amount double,
        record_insert_dt timestamp
)
PARTITIONED BY (file_name string)
stored as orc
tblproperties(
        'orc.compress'='SNAPPY',
        'orc.row.index.stride'='50000',
        'orc.stripe.size'='536870912',
        'transient.lastDdlTime'='1506095272'
);
