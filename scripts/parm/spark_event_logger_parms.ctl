jobInputPath="/userapps/analytics/"
appLocalRootDir=/app/hworks/trade_events
appLocalSettingsDir=/app/hworks/trade_events/scripts/parm
hiveRootDir=/apps/hive/warehouse
debugMode=true
yarnQueue=default
tradeEventTableName=TRADE_EVENTS_FCT
queue=scheduled_default
storageLevel=DUMMY

app-jar=spark-apps/lib/event-logger-1.0.jar
app-main-class=com.trade.EventLoggerMain

driver-memory=4G
num-executors=16
executor-memory=10G
executor-cores=4

jars = "/app/hworks/trade_events/spark-apps/lib/ojdbc7.jar,/app/hworks/trade_events/spark-apps/lib/slf4j-api-1.7.5.jar,/app/hworks/trade_events/spark-apps/lib/slf4j-log4j12-1.7.5.jar,/app/hworks/trade_events/spark-apps/lib/typesafe-config-2.10.1.jar"
driver-java-options="-Dlog4j.configuration=file:///app/hworks/trade_events/spark-apps/lib/log4j.properties"

config-properties="spark.yarn.executor.memoryOverhead=1024,spark.network.timeout=700s,spark.driver.maxResultSize=4g,spark.executor.extraClassPath=/app/hworks/trade_events/spark-apps/lib/*"

hive-jdbc-conn-file=conn_hive_jdbc
