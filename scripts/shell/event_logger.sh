#!/bin/bash
#--------------------------------------------------------------------------
#Script      : event_logger.sh
#
# Description : Checks Parameter File and Executes Spark2 Apps
#
# Usage       : spark2.sh <CONTEXT>
#
#               CONTEXT is the middle portion of Parm File Name which uniquely identifies
#                 each parm file
#               e.g. For parm file spark_event_logger_parms.ctl, Script is called as below -
#                        event_logger.sh event_logger
#
# Exit codes : 0 - Succeeded     1 - Failed
#---------------------------------------------------------------------------
set +x
# Directory path to locate Application root folder
APP_BASE_DIR='/app/hworks/trade_events'
THIS_SCRIPT_DIR=`dirname $0`
export SCRIPT_NM=`basename $0`
CONTEXT="$1"

APP_BASE_DIR="$HOME"

function getParmValue {
   PARM_NAME=$1
   TEMP1=`grep -i "$PARM_NAME" $PARM_FILE | cut -d'=' -f2- | tr -d '[:space:]'`
   # Remove Double Quotes from Start and End of Parm if present
   sed -e 's/^"//' -e 's/"$//' <<<"$TEMP1"

}

function logger {
        MSG=$1
        echo `date '+%Y-%m-%d %H:%M:%S'`" $MSG" >> $LOG
}

SCRIPT_NM_WO_EXT="${SCRIPT_NM%.*}"
LOG_FILE_NM=$SCRIPT_NM_WO_EXT"_"${CONTEXT}_`date '+%Y_%m_%d'`".log"
LOG=$APP_BASE_DIR"/LOG/"$LOG_FILE_NM
echo `date '+%Y-%m-%d %H:%M:%S'`" Log File: $LOG"
logger "Script $SCRIPT_NM $CONTEXT Started " >> $LOG

TMP_DIR="$APP_BASE_DIR/TMP"

FLAG_FILEPATH="$TMP_DIR/${SCRIPT_NM_WO_EXT}_${CONTEXT}_currently_running.tmp"
test -f ${FLAG_FILEPATH}
if [[ "$?" -eq 0 ]]; then
    logger "Error - Temp File ${FLAG_FILEPATH} exists. Either a) Another instance of this job is already running OR b) this job was killed ungracefully in last run. If b) is true then, remove file ${FLAG_FILEPATH} and rerun the job"
    exit 1
fi
touch ${FLAG_FILEPATH} &>> $LOG
TOUCH_CMD_STATUS=$?
test -f ${FLAG_FILEPATH}
TEST_CMD_STATUS=$?
if [[ $TOUCH_CMD_STATUS -ne 0 || $TEST_CMD_STATUS -ne 0 ]]; then
    logger "Error - while creating Flag file : ${FLAG_FILEPATH}" >> $LOG
    exit 1
fi

PARM_FILE="$APP_BASE_DIR/scripts/parm/spark_${CONTEXT}_parms.ctl"
test -f $PARM_FILE
if [[ "$?" -ne 0 ]]; then
         logger "Error - Parm File $PARM_FILE does not exist. Either incorrect parm: $CONTEXT has been passed to the script or parm file for this table has not been created."
         rm ${FLAG_FILEPATH} &>> $LOG
         exit 1
fi

APP_JAR=$APP_BASE_DIR/`getParmValue "app-jar"`
APP_MAIN_CLASS=`getParmValue "app-main-class"`
DRIVER_MEMORY=`getParmValue "driver-memory"`
NUM_EXECUTORS=`getParmValue "num-executors"`
EXECUTOR_CORES=`getParmValue "executor-cores"`
EXECUTOR_MEMORY=`getParmValue "executor-memory"`
JARS=`getParmValue "jars"`
CONFIG_PROPERTIES=`getParmValue "config-properties"`
DRIVER_JAVA_OPTIONS=`getParmValue "driver-java-options"`

DRIVER_JAVA_OPTIONS="$DRIVER_JAVA_OPTIONS -DLogFilePath=$LOG"

# If NUM_EXECUTORS is BLANK --> Dynamic Allocation
if [[ ! -z $NUM_EXECUTORS ]]; then
        PARM_NUM_EXECUTORS=" --num-executors $NUM_EXECUTORS "
else
        logger "Using Dynamic Allocation as parm --num-executors has not been specified"
fi

SPARK_CMD="spark-submit --master yarn --deploy-mode client --driver-java-options '$DRIVER_JAVA_OPTIONS'  --jars $JARS --class $APP_MAIN_CLASS --driver-memory $DRIVER_MEMORY $PARM_NUM_EXECUTORS --executor-cores $EXECUTOR_CORES --executor-memory $EXECUTOR_MEMORY"


# Add Config Properties one by one to Spark Comman
IFS=","
for CONFIG_PARM in $CONFIG_PROPERTIES
do
       SPARK_CMD="$SPARK_CMD --conf $CONFIG_PARM"
done;
# Restore Default value of IFS
unset IFS

#SPARK_CMD="$SPARK_CMD $APP_JAR $APP_BASE_DIR $PARM_FILE &>> $LOG"
SPARK_CMD="$SPARK_CMD $APP_JAR $PARM_FILE &>> $LOG"

logger "Executing Spark Cmd: $SPARK_CMD"
export SPARK_MAJOR_VERSION=2

#Execute Command to Start Spark Application
eval $SPARK_CMD &>> $LOG

eval SPARK_STATUS=$?

test -f ${FLAG_FILEPATH}
if [[ "$?" -ne 0 ]]; then
    logger "Error - Flag File ${FLAG_FILEPATH} does not exist"
    exit 1
fi

rm ${FLAG_FILEPATH} &>> $LOG
RM_CMD_STATUS=$?
test -f ${FLAG_FILEPATH}
TEST_CMD_STATUS=$?
if [[ $RM_CMD_STATUS -ne 0 || $TEST_CMD_STATUS -eq 0 ]]; then
    logger "Error - while removing Flag File ${FLAG_FILEPATH}"
    exit 1
fi

find "${APP_BASE_DIR}/LOG/*.log" -mtime 30 -exec rm {} \;
if [[ $? -eq "0" ]]; then
        logger "LOG files deleted successfully."
fi

exit 0

