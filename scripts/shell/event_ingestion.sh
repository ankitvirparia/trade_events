#!/bin/bash
#--------------------------------------------------------------------------
#
# Script        : event_ingestion.sh
# Description   : Upload Daily Data file of Trade Events to HDFS location
#
# Usage         : ./event_ingestion.sh
#
#
# Exit Code     : 0 - Success
#                 1 - Error in extracting data
#
#---------------------------------------------------------------------------
set +x

# Directory path to locate Application root folder
APP_BASE_DIR='/app/hworks/trade_events'

# Generating Run Date from System Date
RUN_DATE=`date +%Y-%m-%d`

# Preparing log file name from RUN_DATE
LOG_CREATE_DATE=`date +%Y_%m_%d`
LOG_FILE_NM='EVENT_INGESTION_'${LOG_CREATE_DATE}'.log'

# Directory path for Data Landing from Source System
DATA_IN_DIR="$APP_BASE_DIR/IN/"
# Directory path for storing info files on HDFS
DATA_HDFS_DIR="/userapps/trade_events/"
# Directory path for Data Landing from Source System
DATA_ARCHIVE_DIR="$APP_BASE_DIR/Archive/"

# Log folder path
LOG_FILE_PATH="$APP_BASE_DIR/LOG"
LOG_FILE_PATH_WITH_NAME="$APP_BASE_DIR/LOG/"${LOG_FILE_NM}
LOG_FILE_PATTERN='*.log'

# Write Logs to file
echolog() { 
    echo $1 >> ${LOG_FILE_PATH_WITH_NAME} 
}

echolog "Run Date: $RUN_DATE" >> ${LOG_FILE_PATH_WITH_NAME}

#---------------------------------------------------------------------------
# Clean files from previous run
echolog "Deleting all log files older than 7days"
find ${LOG_FILE_PATH} -type f -name '*.log' -mtime +7 -exec rm {} \;

echolog "Deleting all data files from HDFS used in previous run"
hadoop fs -rm -R ${DATA_HDFS_DIR}/*
#---------------------------------------------------------------------------

echolog "Moving data files to HDFS Directory ${DATA_HDFS_DIR}"
hadoop fs -put -f ${DATA_IN_DIR}/*.[Tt][Xx][Tt] ${DATA_HDFS_DIR}
#find ${DATA_IN_DIR} -name "*.[Tt][Xx][Tt]" | xargs -n 500 echo | xargs -i bash -c 'hadoop fs -put -f {} /userapps/trade/tmp'

        
echolog "Move all uploaded files to Archive folder"
mv ${DATA_IN_DIR}/* ${DATA_ARCHIVE_DIR}

echolog "Exit 0"
exit 0
